console.log("repetição while");
let numero = 0;
while(numero<=10){
    
    if(numero%2==0){
        console.log(`valor nr: ${(numero)} é PAR!`);
    }else{
        console.log(`valor nr: ${(numero)}`);
    }
    numero++; 
}

let numero1 = 0;
console.log("REPETIÇÃO DO/WHILE");
do{
    console.log(`Valor nr: ${(numero1)}`);
    numero1++;
}while(numero1<=0);
