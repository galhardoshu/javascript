let nome = "Filipe Galhardo";
let idade = 23;
let isProfessor = false;

console.log("Tipo da variável nome: " + typeof(nome));
console.log("Tipo da variável idade: " + typeof(idade));
console.log(`Tipo da variável isProfessor: ${typeof(isProfessor)}`);
